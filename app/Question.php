<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Question extends Model
{   
    /**
     * The attributes that are mass assignable.
     * 
     */
    protected $fillable = [
        'question_title',
    ];

    /**
     * The question belongs to a questionnaire.
     * 
     */
    public function questionnaire()
    {
        return $this->belongsTo('App\Questionnaire', 'questionnaire_id', 'id');
    }
    /**
     * The question has many answers.
     * 
     */
    public function answers()
    {
        return $this->hasMany('App\Answer', 'id', 'question_id');
    }
}
