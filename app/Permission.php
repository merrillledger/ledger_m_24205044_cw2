<?php

 namespace App;

 use Illuminate\Database\Eloquent\Model;

 class Permission extends Model
 {  
     /**
     * persmissions belong to many roles
     * 
     */
     public function roles() {
         return $this->belongsToMany(Role::class);
     }
 }