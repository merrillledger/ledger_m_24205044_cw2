<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    /**
     * The attributes that are mass assignable.
     * 
     */
    protected $fillable = [
        'option',
    ];

    /**
     * The answers belong to the question.
     * 
     */
    public function question()
    {
        return $this->belongsTo('App\Question', 'question_id' , 'id');
    }

    /**
     * The answers that has many responses.
     * 
     */
    public function responses()
    {
        return $this->hasMany('App\Responses', 'answer_response', 'response_id', 'answer_id');
    }
}

