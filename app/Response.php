<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
    /**
     * The response have many answers
     */
    public function answers()
    {
        return $this->belongsToMany('App\Answers', 'answer_response', 'response_id', 'answer_id');
    }
}
