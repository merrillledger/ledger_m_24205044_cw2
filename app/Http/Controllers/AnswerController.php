<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question; //Question model is it linked
use App\Answer; //Answer model is it linked
use Auth; //Auth model is it linked


class AnswerController extends Controller
{
     /*
    * Secure the set of pages to the admin.
    */
    public function __construct()
    {
      $this->middleware('auth'); //ensuring you have to be logged in to access the url
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get all the answers
        $answers = Answer::all();

        //returns the answer view and pulls in the answers
        return view('admin/answers', ['answers' => $answers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //pulls in the option and the id and sets it to the ans variable
        $ans = Answer::pluck('option', 'id');

        // now we can return to the create view and pulls in the ans
        return view('admin/answers/create', compact('ans'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validates the fields
        $this->validate($request, [
            'option' => 'required',
        ]);

        //The store method creates a answer and stores it 
        $answer = Answer::create($request->all());

        //redirects to the answer
        return redirect('admin/answers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         //answers finds id first
        $answer = Answer::where('id',$id)->first();

        // if answer does not exist return to list
        if(!$answer)
        {
            return redirect('/admin/answers/show'); //redirect to the show
        }
        
        return view('/admin/answers/show')->withAnswer($answer);//show view or pulling in answer
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Find or fail the answer id for the edit blade
        $answer = Answer::findOrFail($id);

        //Return the edit view and pull in the answer
        return view('admin/answers/edit', compact('answer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Find or fail the id on the answer
        $answer = Answer::findOrFail($id);

        //Updates the data
        $answer->update($request->all());

        //Redirects to the answer view
        return redirect('admin/answers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //find or fail the answer
        $answer = Answer::find($id);

        //delete the answer
        $answer->delete();

        //redirect to the answer blade
        return redirect('admin/answers');
    }
}
