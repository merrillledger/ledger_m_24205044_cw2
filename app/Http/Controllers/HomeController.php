<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Questionnaire; //Questionnaire model is it linked

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth'); //ensuring you have to be logged in to access the url
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // get all the questionnaires
        $questionnaire = Questionnaire::all();

        //returns the home view and pulls in the questionnaires
        return view('home', ['questionnaires' => $questionnaire]);
    }
}
