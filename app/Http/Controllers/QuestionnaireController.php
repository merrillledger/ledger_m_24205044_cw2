<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Questionnaire; //Questionnaire model is it linked
use App\Question; //Question model is it linked
use Auth; //Auth model is it linked


class QuestionnaireController extends Controller
{
    /*
   * Secure the set of pages to the admin.
   */
    public function __construct()
    {
        $this->middleware('auth'); //ensuring you have to be logged in to access the url
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // get all the questionnaires
       $questionnaires = Questionnaire::get();

       //returns the questionnaire view and pulls in the questionnaires
       return view('admin/questionnaires', ['questionnaires' => $questionnaires]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //pulls in the question_title and the id and sets it to the opt variable
        $quests = Question::pluck('question_title', 'id');

        // now we can return to the create view and pulls in the quests
        return view('admin/questionnaires/create', compact('quests'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validates the fields
        $this->validate($request, [
            'title' => 'required',
            'information' => 'required',
        ]);
        
        /**
         * creates a questionnaire and links in the user_id and saves it
         */
        $questionnaire = Questionnaire::create($request->all());
        $questionnaire->user_id = Auth::user()->id;
        $questionnaire->save();
        
        //redirects to the questionnaire id url
        return redirect('admin/questionnaires/'.$questionnaire->id);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //questionnaire finds the user and question id first
        $questionnaires = Questionnaire::with('user', 'questions')->where('id',$id)->first();
        
        // if questionnaires does not exist return to list
        if(!$questionnaires)
        {
            return redirect('/admin/questionnaires/show'); //redirect to the show
        }
        return view('/admin/questionnaires/show')->withQuestionnaire($questionnaires);//show view or pulling in questionnaire
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Find or fail the questionnaire id for the edit blade
        $questionnaire = Questionnaire::findOrFail($id);

        //Return the edit view and pull in the questionnaire
        return view('admin/questionnaires/edit', compact('questionnaire'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Find or fail the id on the questionnaire
        $questionnaire = Questionnaire::findOrFail($id);

        //Updates the data
        $questionnaire->update($request->all());

        //Redirects to the questionnaire view
        return redirect('admin/questionnaires');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //find or fail the questionnaire
        $questionnaire = Questionnaire::find($id);

        //delete the questionnaire
        $questionnaire->delete();

        //redirect to the questionnaire blade
        return redirect('admin/questionnaires');
    }
}