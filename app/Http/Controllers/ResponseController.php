<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Questionnaire; //Questionnaire model is it linked
use App\Response; //Response model is it linked
use App\Answer; //Answer model is it linked

class ResponseController extends Controller
{
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Questionnaire $questionnaire)//Model binding the questionnaire
    {
       // get all the questionnaires
       $responses = Response::all();

       //return view that pulls in questionnaires
       return view('admin/live', ['questionnaire' => $questionnaire]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //pulls in the option and the id and sets it to the opt variable
        $opt = Answer::pluck('option', 'id');

        // now we can return the data with the view
        return view('admin/live', compact('opt'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [ //Validates the input
            'responses.*.answer_id' => 'required',
        ]);
        
    
        
        $responses = Response::create($request->all());//The store method creates a response and stores it 
        $responses->answers()->attach($request->input('options'));//pulls in the option id

        return redirect('admin/live'); //redirects to the live view
    }

    public function show(Questionnaire $questionnaire) //model binding the questionnaire
    {
        $questionnaire->load('questions.answers'); //concating the questions and answers and lazy loading them
        
        return view('/admin/live', compact('questionnaire'));//return the live view and pulling in the questionnaire
    }


}
