<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Questionnaire;  //Questionnaire model is it linked
use App\Question;//Question model is it linked
use Auth; //Auth model is it linked


class QuestionController extends Controller
{
     /*
    * Secure the set of pages to the admin.
    */
    public function __construct()
    {
      $this->middleware('auth'); //ensuring you have to be logged in to access the url
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // get all the questions
        $questions = Question::all();

        //returns the question view and pulls in the questions
        return view('admin/question', ['question' => $questions]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         // get all the questionnaires
        $questionnaire = Questionnaire::all();
         
        //returns the questions create pulls in the questionnaires
        return view('admin/question/create', compact('questionnaire'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //vallidates the fields
        $this->validate($request, [
            'question_title' => 'required',
        ]);
        
        //The store method creates a question and stores it 
        $question = Question::create($request->all());

        //Redirects to the question view
        return redirect('admin/question');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //question finds id 
        $question = Question::where('id', $id)->get();

        // if question does not exist return to list
        if(!$question) 
        {
            return redirect('/admin/question/show');
        }
        
        return view('/admin/question/show')->withQuestion($question);//return view with question on the question show view
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Find or fail the question id for the edit blade
        $question = Question::findOrFail($id);

        //Return the edit view and pull in the question
        return view('admin/question/edit', compact('question'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Find or fail the id on the question
        $question = Question::findOrFail($id);

        //Updates the data
        $question->update($request->all());

        //Redirects to the question view
        return redirect('admin/question');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //find or fail the question
        $question = Question::find($id);

        //delete the question
        $question->delete();

        //redirect to the question blade
        return redirect('admin/question');
    }
}