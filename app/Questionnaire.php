<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Questionnaire extends Model
{
    /**
     * The attributes that are mass assignable.
     * 
     */
    protected $fillable = [
        'title',
        'information',
    ];

    /**
     * Get the user associated with the given questionnaire
     *
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo('app\User','user_id', 'id');
    }

    /**
     * The questionnaires have many questions.
     */
    public function questions()
    {
        return $this->hasMany('App\Question', 'id', 'questionnaire_id');
    }

}