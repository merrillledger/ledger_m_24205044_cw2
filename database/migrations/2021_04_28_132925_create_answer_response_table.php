<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnswerResponseTable extends Migration
{
    /**
     * 
     *Creates a migration pivot table for the answers and responses.
     * @return void
     */
    public function up()
    {
        Schema::create('answer_response', function (Blueprint $table) {
            $table->integer('answer_id')->unsigned();//creates a composite key with answer_id and response_id
            $table->integer('response_id')->unsigned();

            $table->foreign('answer_id')->references('id')->on('questions')->onDelete('cascade');
            $table->foreign('response_id')->references('id')->on('responses')->onDelete('cascade');

            $table->primary(['answer_id', 'response_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answer_response');
    }
}
