<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome'); //shows welcome page
});

Route::get('/admin/live', 'ResponseController@show'); //this registers the route outside of the auth so that it can be accessed by anyone

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
Route::group(['middleware' => ['web']], function () {
    Route::auth();  //route for authentication

    Route::get('/home', 'HomeController@index');
    Route::resource('/admin/questionnaires', 'QuestionnaireController'); //route for questionnaire
    Route::resource('/admin/question', 'QuestionController'); //route for question
    Route::resource('/admin/answers', 'AnswerController'); //route for answer
    
});
