<?php
$I = new FunctionalTester($scenario);

$I->am('unsecured');
$I->wantTo('submit a questionnaire response');

  // Add db test data

// When
$I->amOnPage('/admin/questionnaires/response');
$I->see('Submit a response', 'h1');

$I->submitForm('#createresponse', [
  'question' => 'Response1',
  'question' => 'Response2',
  'question' => 'Response3',
  'question' => 'Response4',
  'question' => 'Response5',
]);

// Then
$I->seeCurrentUrlEquals('/admin/welcome');
$I->see('Thankyou', 'h1');
$I->see('Thankyou for you response!');