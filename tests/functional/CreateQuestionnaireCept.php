<?php
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('create a new questionnaire');

// log in as your admin user
  // This should be id of 1 if you created your manual login for a known user first.
  Auth::loginUsingId(1);
  // Add db test data

// add a test user
$I->haveRecord('users', [
  'id' => '9999',
  'name' => 'testuser1',
  'email' => 'test1@user.com',
  'password' => 'password',
]);

// add a test questionnaire to check that content can be seen in list at start

$I->haveRecord('questionnaires', [
  'id' => '9000',
  'title' => 'Questionnaire 1',
  'information' => 'Questionnaire information',
  'questionnaire_live' => 'yes',
  'user_id' => 9999
]);



//tests ///////
//create a questionnaire linked to user

// When
$I->amOnPage('/admin/questionnaires');
$I->see('Questionnaires', 'h1');
$I->dontSee('Questionnaire 2');
// And
$I->click('Create Questionnaire');

// Then
$I->amOnPage('/admin/questionnaires/create');
// And
$I->see('Create Questionnaire', 'h1');
$I->submitForm('.createquestionnaire', [
    'title' => 'Questionnaire 2',
    'information' => 'Questionnaire info',
    'questionnaire_live' => 'yes',
]);

$user = $I->grabRecord('questionnaires', ['title' => 'Questionnaire 2']);

// Then
$I->seeCurrentUrlEquals('/admin/questionnaires');
$I->see('Questionnaires', 'h1');
$I->see('New questionnaire added!');
$I->see('Randomtest');

$I->click('Questionnaire 2'); // the title is a link to the questionnaire page


// Check that the url has a similar path to this.. the last part is a regular expression to allow for a digit or more to be returned as an id.
$I->seeCurrentUrlMatches('~/admin/articles/preview');
$I->see('Questionnaire 2', 'h1');
$I->see('questionnaire info');
$I->see('creator: testuser1'); // Got to here in Passing first BBd test 4 errors on line 24
$I->see('Yes');


// create an questionnaire linked to a user
// When
$I->amOnPage('/admin/questionnaires');
$I->see('Questionnaire', 'h1');
$I->see('Questionnaire 2');