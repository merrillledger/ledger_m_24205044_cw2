<?php
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('create a new question');

// log in as your admin user
  // This should be id of 1 if you created your manual login for a known user first.
  Auth::loginUsingId(1);
  // Add db test data

// add a test user
$I->haveRecord('users', [
  'id' => '9999',
  'name' => 'testuser1',
  'email' => 'test1@user.com',
  'password' => 'password',
]);

// add a test questionnaire to check that content can be seen in list at start

$I->haveRecord('questionnaires', [
  'id' => '9000',
  'title' => 'Questionnaire 1',
  'information' => 'Questionnaire information',
  'user_id' => 9999
]);

$I->haveRecord('questions', [
    'id' => '9000',
    'question_title' => 'Question 1',
    'questionnaire_id' => 9999
  ]);



//tests ///////
//create a questionnaire linked to user

// When
$I->amOnPage('/admin/questionnaires');
$I->see('Questionnaires', 'h1');
$I->see('Questionnaires 1');
// And
$I->click('Questionnaire 1');

// Then
$I->amOnPage('/admin/questionnaires/show');
// And
$I->click('Add question');

// Then
$I->amOnPage('/admin/questionnaires/create');
// And
$I->see('Create Question', 'h1');
$I->submitForm('.createquestion', [
    'question_title' => 'Question 1',
]);

$questionnaire = $I->grabRecord('questions', ['question' => 'Question 1']);

// Then

// create an questionnaire linked to a user
// When
$I->amOnPage('/admin/questionnaires');
$I->see('Questionnaire', 'h1');
$I->see('Questionnaire 1');