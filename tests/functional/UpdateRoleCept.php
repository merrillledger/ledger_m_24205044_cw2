<?php
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('update a role');

// log in as your admin user
  // This should be id of 1 if you created your manual login for a known user first.
  Auth::loginUsingId(1);
  // Add db test data

// create a role in the db that we can then update
$I->haveRecord('users', [
        'id' => '9999',
        'name' => 'testuser',
        'label' => 'testlabel',
    ]);

// Check the role is in teh db and can be seen
$I->seeRecord('roles', ['name' => 'testrole', 'id' => '9999']);


// When
$I->amOnPage('/admin/roles');

// then

    // Check  the link is present - this is because there could potentially be many update links/buttons.
    // each link can be identified by the role id .
$I->seeElement('a', ['name' => '9999']);
// And
$I->click('a', ['name' => '9999']);

// Then
$I->amOnPage('/admin/roles/9999/edit');
// And
$I->see('Edit User - testrole', 'h1');

// And
$I->amGoingTo('Clear the name field in order to try and submit an invalid entry');

// When
$I->fillField('name', null);
// And
$I->click('Update Role');

// the above could and should be expanded to run a separate test on each form field!

// Then
$I->expectTo('See the form again with errors identified');
$I->seeCurrentUrlEquals('/admin/roles/9999/edit');
$I->see('the name filed is required');
// Then
$I->fillField('name', 'updatedrole');
// And
$I->click('Update Role');

// Then
$I->seeCurrentUrlEquals('admin/roles');
$I->seeRecord('roles', ['name' => 'updatedrole']);
$I->see('Roles', 'h1');
$I->see('updatedrole');
