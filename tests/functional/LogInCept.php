<?php
$I = new FunctionalTester($scenario);
$I->am('a User');
$I->wantTo('sign in');

// given
$I->amOnPage('/login');
// when
$I->fillField('name', 'merrill');
// and
$I->fillField('email', 'merrill@example.com');
// and
$I->fillField('password', 'qwerty');
// and
$I->click('LOGIN');
// then
$I->see('Welcome, Merrill!');