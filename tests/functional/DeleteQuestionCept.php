<?php
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('delete a question');

// log in as your admin user
  // This should be id of 1 if you created your manual login for a known user first.
  Auth::loginUsingId(1);
  // Add db test data

// create a question in the db that we can then delete it without changing our needed data
$I->haveRecord('questionnaires', [
    'id' => '9999',
    'title' => 'testuser',
    'information' => 'testquestions',
    'user_id' => '9999',
    'published_at' => '12/04/21',
]);

$I->haveRecord('questions', [
    'id' => '9999',
    'question_title' => 'testquestion',
    'questionnaire_id' => '9999',
]);


// Check the question is in teh db and can be seen
$I->seeRecord('questions', ['question' => 'testquestionnaire', 'id' => '9999']);


// When
$I->amOnPage('/admin/questionnaires/show');

// then

// Check  the question is present buttons.
$I->see('question 1');
$I->seeElement('question 1', 'a.item');

// Then
$I->click('question 1 delete');

// Then
$I->seeCurrentUrlEquals('admin/questionnaires/show');

// Check  the question is has been deleted.
$I->dontSee('question 1');
$I->dontSeeElement('question 1', 'a.item');
