<?php

$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('update a question');

// log in as your admin user
  // This should be id of 1 if you created your manual login for a known user first.
  Auth::loginUsingId(1);
  // Add db test data

// create a questionnaire in the db that we can then update
$I->haveRecord('questionnaires', [
    'id' => '9999',
    'title' => 'testuser',
    'information' => 'testquestions',
    'user_id' => '9999',
    'published_at' => '12/04/21',
]);

$I->haveRecord('questions', [
    'id' => '9000',
    'question_title' => 'Question 1',
    'questionnaire_id' => 9999
  ]);

$I->haveRecord('answers', [
    'id' => '9999',
    'option' => 'testanswer',
    'question_id' => '9999',
]);




// Check the questionnaire is in teh db and can be seen
$I->seeRecord('answers', ['answer' => 'Answer 1', 'id' => '9999']);



// When
$I->amOnPage('/admin/questionnaires');

// then

    // Check  the link is present - this is because there could potentially be many update links/buttons.
    // each link can be identified by the questionnaire id .
$I->seeElement('a', ['title' => '9999']);
// And
$I->click('a', ['title' => '9999']);

// Then
$I->amOnPage('/admin/answer/9999/edit');
// And
$I->see('Edit Answer - answer 1', 'h1');

// And
$I->amGoingTo('Clear the title field in order to try and submit an invalid entry');

// When
$I->fillField('title', null);
// And
$I->click('Update Answer');

// the above could and should be expanded to run a separate test on each form field!

// Then
$I->expectTo('See the form again with errors identified');
$I->seeCurrentUrlEquals('/admin/answers/9999/edit');
$I->see('the option filed is required');
// Then
$I->fillField('answer', 'updatedanswer');
// And
$I->click('Update Answer');

// Then
$I->seeCurrentUrlEquals('admin/answers');
$I->seeRecord('answer', ['answer' => 'updatedanswer']);
$I->see('Answer', 'h1');
$I->see('updatedanswer');
