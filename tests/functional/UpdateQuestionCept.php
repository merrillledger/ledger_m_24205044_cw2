<?php
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('update a question');

// log in as your admin user
  // This should be id of 1 if you created your manual login for a known user first.
  Auth::loginUsingId(1);
  // Add db test data

// create a question in the db that we can then update
$I->haveRecord('questionnaires', [
    'id' => '9999',
    'title' => 'testuser',
    'information' => 'testquestions',
    'user_id' => '9999',
    'published_at' => '12/04/21',
]);

$I->haveRecord('questions', [
    'id' => '9000',
    'question_title' => 'Question 1',
    'questionnaire_id' => 9999
  ]);


// Check the question is in teh db and can be seen
$I->seeRecord('questions', ['title' => 'testquestion', 'id' => '9999']);



// When
$I->amOnPage('/admin/question');

// then

    // Check  the link is present - this is because there could potentially be many update links/buttons.
    // each link can be identified by the question id as name.
$I->seeElement('a', ['question' => '9999']);
// And
$I->click('a', ['question' => '9999']);

// Then
$I->amOnPage('/admin/question/9999/edit');
// And
$I->see('Edit Question - testquestion', 'h1');

// And
$I->amGoingTo('Clear the question field in order to try and submit an invalid entry');

// When
$I->fillField('question', null);
// And
$I->click('Update Question');

// the above could and should be expanded to run a separate test on each form field!

// Then
$I->expectTo('See the form again with errors identified');
$I->seeCurrentUrlEquals('/admin/questions/9999/edit');
$I->see('the question filed is required');
// Then
$I->fillField('question', 'updatedquestion');
// And
$I->click('Update Question');

// Then
$I->seeCurrentUrlEquals('admin/questions');
$I->seeRecord('question', ['title' => 'updatedquestion']);
$I->see('Question', 'h1');
$I->see('updatedquestion');
