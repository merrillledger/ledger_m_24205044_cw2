<?php
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('delete a answer');

// log in as your admin user
  // This should be id of 1 if you created your manual login for a known user first.
  Auth::loginUsingId(1);
  // Add db test data

// create a user in the db that we can then delete it without changing our needed data
$I->haveRecord('questionnaires', [
    'id' => '9999',
    'title' => 'testuser',
    'information' => 'testquestions',
    'user_id' => '9999',
    'published_at' => '12/04/21',
]);

$I->haveRecord('questions', [
    'id' => '9999',
    'question_title' => 'testquestion',
    'questionnaire_id' => '9999',
]);

$I->haveRecord('answers', [
    'id' => '9999',
    'option' => 'testanswer',
    'answer_id' => '9999',
]);

// Check the answers is in teh db and can be seen
$I->seeRecord('answers', ['answer' => 'answer1', 'id' => '9999']);


// When
$I->amOnPage('/admin/questionnaires/show');

// then

// Check  the answers is present buttons.
$I->see('answer1');
$I->seeElement('answer1', 'a.item');

// Then
$I->click('answer1 delete');

// Then
$I->seeCurrentUrlEquals('admin/questionnaires/show');

// Check  the answers is has been deleted.
$I->dontSee('answer1');
$I->dontSeeElement('answer1', 'a.item');
