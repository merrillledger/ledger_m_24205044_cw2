<?php
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('update a questionnaire');

// log in as your admin user
  // This should be id of 1 if you created your manual login for a known user first.
  Auth::loginUsingId(1);
  // Add db test data

// create a answers in the db that we can then update
$I->haveRecord('questionnaires', [
    'id' => '9999',
    'title' => 'testuser',
    'information' => 'testquestions',
    'user_id' => '9999',
    'published_at' => '12/04/21',
]);

// Check the answers is in teh db and can be seen
$I->seeRecord('questionnaires', ['title' => 'testquestionnaire', 'id' => '9999']);



// When
$I->amOnPage('/admin/questionnaires');

// then

    // Check  the link is present - this is because there could potentially be many update links/buttons.
    // each link can be identified by the answers id .
$I->seeElement('a', ['title' => '9999']);
// And
$I->click('a', ['title' => '9999']);

// Then
$I->amOnPage('/admin/questionnaires/9999/edit');
// And
$I->see('Edit Questionnaire - testquestionnaire', 'h1');

// And
$I->amGoingTo('Clear the title field in order to try and submit an invalid entry');

// When
$I->fillField('title', null);
// And
$I->click('Update Questionnaire');

// the above could and should be expanded to run a separate test on each form field!

// Then
$I->expectTo('See the form again with errors identified');
$I->seeCurrentUrlEquals('/admin/questionnaires/9999/edit');
$I->see('the name filed is required');
// Then
$I->fillField('name', 'updatedquestionnaire');
// And
$I->click('Update Questionnaire');

// Then
$I->seeCurrentUrlEquals('admin/questionnaires');
$I->seeRecord('questionnaire', ['title' => 'updatedquestionnaire']);
$I->see('Questionnaires', 'h1');
$I->see('updatedquestionnaire');
