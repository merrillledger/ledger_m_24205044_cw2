<?php 
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('create a new user');

// When
$I->amOnPage('/admin/login');

// And
$I->click('Register');

// Then
$I->amOnPage('/admin/register');
// And
$I->see('Register', 'h1');
$I->submitForm('#createuser', [
    'name' => 'Merrill Ledger',
    'email' => 'ledgerm@edgehill.ac.uk',
    'password' => 'password'
]);
// Then
$I->seeCurrentUrlEquals('/admin/login');
$I->see('Users', 'h1');
$I->see('New user added!');
$I->see('Merrill Ledger');



// Check for duplicates

// When
$I->amOnPage('/admin/login');
// And
$I->click('Add User');

// Then
$I->amOnPage('/admin/register');
// And
$I->see('Add User', 'h1');
$I->submitForm('#createuser', [
    'name' => 'Merrill Ledger',
    'email' => 'ledgerm@edgehill.ac.uk',
    'password' => 'password'
]);
// Then
$I->seeCurrentUrlEquals('/admin/register');
$I->see('Users', 'h1');
$I->see('Error user already exists!');