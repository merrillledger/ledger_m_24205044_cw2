<?php
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('view a questionnaire responses');

// log in as your admin user
  // This should be id of 1 if you created your manual login for a known user first.
  Auth::loginUsingId(1);
  // Add db test data

// When
$I->amOnPage('/admin/questionnaires');
$I->see('Questionnaires', 'h1');

// And
$I->click('View Questionnaire Responses');

// Then
$I->amOnPage('/admin/questionnaires/view');
// And
$I->see('View Questionnaire Responses', 'h1');
$I->seeRecord('responses', ['submitted_response' => 'testresponse', 'id' => '9999', 'submitted_at' => '12/04/21']);

// Then
$I->seeCurrentUrlEquals('/admin/questionnaires');
$I->see('Questionnaires', 'h1');