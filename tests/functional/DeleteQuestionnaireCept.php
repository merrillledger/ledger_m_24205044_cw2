<?php
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('delete a questionnaire');

// log in as your admin user
  // This should be id of 1 if you created your manual login for a known user first.
  Auth::loginUsingId(1);
  // Add db test data

// create a questionnaire in the db that we can then delete it without changing our needed data
$I->haveRecord('questionnaires', [
    'id' => '9999',
    'title' => 'testuser',
    'information' => 'testquestions',
    'user_id' => '9999',
    'published_at' => '12/04/21',
]);

// Check the questionnaire is in teh db and can be seen
$I->seeRecord('questionnaires', ['title' => 'testquestionnaire', 'id' => '9999']);


// When
$I->amOnPage('/admin/questionnaires');

// then

// Check  the questionnaire is present buttons.
$I->see('testquestionnaire');
$I->seeElement('testquestionnaire', 'a.item');

// Then
$I->click('testquestionnaire delete');

// Then
$I->seeCurrentUrlEquals('admin/questionnaires');

// Check  the questionnaire is has been deleted.
$I->dontSee('testquestionnaire');
$I->dontSeeElement('testquestionnaire', 'a.item');
