<?php
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('delete a role');

// log in as your admin user
  // This should be id of 1 if you created your manual login for a known user first.
  Auth::loginUsingId(1);
  // Add db test data

// create a role in the db that we can then delete it without changing our needed data
$I->haveRecord('roles', [
    'id' => '9999',
    'name' => 'testrole',
    'label' => 'testlabel',
]);

// Check the role is in teh db and can be seen
$I->seeRecord('roles', ['name' => 'testrole', 'id' => '9999']);


// When
$I->amOnPage('/admin/roles');

// then

// Check  the role is present buttons.
$I->see('testrole');
$I->seeElement('testrole', 'a.item');

// Then
$I->click('testrole delete');

// Then
$I->seeCurrentUrlEquals('admin/roles');

// Check  the role is has been deleted.
$I->dontSee('testrole');
$I->dontSeeElement('testrole', 'a.item');
