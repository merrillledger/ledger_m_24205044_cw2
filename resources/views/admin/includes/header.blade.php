<nav class="top-bar">
      <div class="container">
        <div class="top-bar-left">
        <ul class="nav navbar-nav">
            <a class="navbar-brand" href="#">Questionnaire Company</a>
            <li role="menuitem"><a href="{{ route('home') }}">Dashboard</a></li>
            <li role="menuitem"><a href="{{ route('questionnaires') }}">Questionnaires</a></li>
          </ul>
        </div>
        <div class="top-bar-right">
        <ul class="nav navbar-nav">
            <li role="menuitem"><a href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                           Logout
                                       </a>

                                       <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                           {{ csrf_field() }}
                                       </form></li>
          </ul>
        </div>
      </div>
    </nav>