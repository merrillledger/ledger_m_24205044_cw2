@extends('layouts.app')<!--This calls the layout app for all the pages-->

@section('title', 'Show Question')<!--This adds the title for the page-->

@section('content')<!--Inserts the content-->

    <h1>Add answer to the question</h1><!--Title-->

        @if (isset ($questions)) <!--If functions that add shows any questions-->
                <ul>
                   @foreach ($questions as $question)
                    <li><a href="/admin/question/{{ $question->id }}" name="{{ $answer->id }}">{{ $question->question_title }}</a>{!! Session::has('msg') ? Session::get("msg") : '' !!}</li><!--Turns questions into links-->
                   @endforeach
                </ul>
            @else<!--Else no answers added-->
             <p> no questions added yet! </p><!--Gives a message that shows no questions added-->
        @endif

    {{ Form::open(array('action' => 'AnswerController@create', 'method' => 'get')) }}<!--Adds a button that takes the user to the create answer page-->
        <div class="row">
            {!! Form::submit('Add Answer', ['class' => 'button']) !!}
        </div>
    {{ Form::close() }}
 
@endsection