@extends('layouts.app') <!--This calls the layout app for all the pages-->

@section('title', 'Create New Question') <!--This adds the title for the page-->

@section('content')<!--Inserts the content-->

    <h1>Add Question</h1> <!--Title-->
    
          <!-- errors -->
          @if ($errors->any())
              <div>
                  <ul class="alert alert-danger">
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif

        <!--Form that add the question to the database-->
    {!! Form::open(array('action' => 'QuestionController@store', 'id' => 'createquestion')) !!}
            {{ csrf_field() }}<!--Secures the form and stops page from expiring-->
            
        <div class="row large-12 columns">
        {!! Form::label('question_title', 'Question:') !!}
        {!! Form::text('question_title', null, ['class' => 'large-8 columns']) !!}<!--Adds a text field that adds to the question_title field-->
        </div>

         <div class="row large-4 columns">
            {!! Form::submit('Add Question', ['class' => 'button']) !!}<!--Submits the form-->
        </div>
    {!! Form::close() !!}

@endsection
