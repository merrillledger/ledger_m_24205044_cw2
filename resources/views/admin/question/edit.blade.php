@extends('layouts.app') <!--This calls the layout app for all the pages-->

@section('title', 'Edit Question')<!--This adds the title for the page-->

@section('content')<!--Inserts the content-->

    <h1>Edit Question - {{ $question->question_title }}</h1> <!--Title-->

        <!-- errors -->
         @if ($errors->any())
              <div>
                  <ul class="alert alert-danger">
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif

     <!--Form that edits the question in the database-->
    {!! Form::model($question, ['method' => 'PATCH', 'url' => 'admin/question/'.$question->id]) !!}
            {{ csrf_field() }}<!--Secures the form and stops page from expiring-->
            
        <div class="row large-12 columns">
            {!! Form::label('question_title', 'Question:') !!}
            {!! Form::text('question_title', null, ['class' => 'large-8 columns']) !!}<!--allows an edit text-->
        </div>

         <div class="row large-4 columns">
            {!! Form::submit('Update Question', ['class' => 'button']) !!}<!--Sumbit button-->
        </div>

    {!! Form::close() !!}

@endsection