@extends('layouts.app')<!--This calls the layout app for all the pages-->

@section('title', 'Questionnaire')<!--This adds the title for the page-->

@section('content')<!--Inserts the content-->

    <h1>Questionnaires</h1> 

        @if (isset ($questionnaires))<!--If functions that add shows any questionnaires-->

            <ul>
                @foreach ($questionnaires as $questionnaire)<!--For each loop that shows each of the questionnaire-->
                    <li><a href="/admin/questionnaires/{{ $questionnaire->id }}" name="{{ $questionnaire->title }}">{{ $questionnaire->title }}</a></li><!--Turns questionnaire into links-->
                    <li><a href="/admin/questionnaires/{{ $questionnaire->id }}/edit" name="{{ $questionnaire->title }}">Edit</a></li><!--Links that allows you to edit-->
                    <li>
                        {!! Form::open(['method' => 'DELETE', 'route' => ['questionnaires.destroy', $questionnaire->id]]) !!}<!--Button that deletes the question-->
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                        {!! Form::close() !!}
                    </li>
                @endforeach
            </ul>
          @else<!--Else no questionnaire added-->
            <p> no questionnaires added yet </p><!--Message that tells you theres no questionnaire-->
        @endif

    {{ Form::open(array('action' => 'QuestionnaireController@create', 'method' => 'get')) }}<!--Button that takes user to the create questionnaire page-->
        <div class="row">
            {!! Form::submit('Add Questionnaire', ['class' => 'button']) !!}
        </div>
    {{ Form::close() }}

@endsection