@extends('layouts.app')<!--This calls the layout app for all the pages-->

@section('title', 'Edit Questionnaire')<!--This adds the title for the page-->

@section('content')<!--Inserts the content-->

    <h1>Edit Questionnaire - {{ $questionnaire->title }}</h1> <!--Title-->

         <!-- errors -->
         @if ($errors->any())
              <div>
                  <ul class="alert alert-danger">
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif

    <!--Form that edits the questionnaire in the database-->
    {!! Form::model($questionnaire, ['method' => 'PATCH', 'url' => 'admin/questionnaires/'.$questionnaire->id]) !!}
            {{ csrf_field() }}<!--Secures the form and stops page from expiring-->
        <div class="row large-12 columns">
            {!! Form::label('title', 'Title:') !!}
            {!! Form::text('title', null, ['class' => 'large-8 columns']) !!}<!--Edits a title field-->
        </div>

        <div class="row large-12 columns">
            {!! Form::label('information', 'Information about questionnaire:') !!}
            {!! Form::textarea('information', null, ['class' => 'large-8 columns']) !!}<!--Edits a information field-->
        </div>

        <div class="row large-4 columns">
            {!! Form::submit('Edit Questionnaire', ['class' => 'button']) !!}<!--Submits the form-->
        </div>
    {!! Form::close() !!}
  
@endsection