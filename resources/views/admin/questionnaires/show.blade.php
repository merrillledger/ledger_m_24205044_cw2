@extends('layouts.app')<!--This calls the layout app for all the pages-->

@section('title', 'Show Questionnaire')<!--This adds the title for the page-->

@section('content')<!--Inserts the content-->

    <h1>{{  $questionnaire->title  }}</h1> <!--Title-->
    <p>{{ $questionnaire->information }}</p> <!--The questionnaire information-->
                               

    {{ Form::open(array('action' => 'QuestionController@create', 'method' => 'get')) }}<!--Adds a button that takes the user to the create question page-->
        <div class="row">
            {!! Form::submit('Add Question', ['class' => 'button']) !!}
        </div>
    {{ Form::close() }}
 
@endsection