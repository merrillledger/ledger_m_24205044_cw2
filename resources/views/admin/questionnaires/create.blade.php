@extends('layouts.app')<!--This calls the layout app for all the pages-->

@section('title', 'Create Questionnaire')<!--This adds the title for the page-->

@section('content')<!--Inserts the content-->

    <h1>Add Questionnaire</h1> <!--Title-->

            <!-- errors -->
         @if ($errors->any())
              <div>
                  <ul class="alert alert-danger">
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif

     <!--Form that add the questionnaire to the database-->
    {!! Form::open(array('action' => 'QuestionnaireController@store', 'id' => 'createquestionnaire')) !!}
            {{ csrf_field() }}<!--Secures the form and stops page from expiring-->

        <div class="row large-12 columns">
            {!! Form::label('title', 'Title:') !!}
            {!! Form::text('title', null, ['class' => 'large-8 columns']) !!}<!--Adds a text field that adds to the title field-->
        </div>

        <div class="row large-12 columns">
            {!! Form::label('information', 'Information about questionnaire:') !!}
            {!! Form::textarea('information', null, ['class' => 'large-8 columns']) !!}<!--Adds a textarea field that adds to the information field-->
        </div>

        <div class="row large-4 columns">
            {!! Form::submit('Add Questionnaire', ['class' => 'button']) !!}<!--Submits the form-->
        </div>
    {!! Form::close() !!}

@endsection