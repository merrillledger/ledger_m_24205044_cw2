@extends('layouts.app')<!--This calls the layout app for all the pages-->

@section('title', 'Answers')<!--This adds the title for the page-->

@section('content')<!--Inserts the content-->

    <h1>Added Answers</h1>  <!--Title-->


        @if (isset ($answers))<!--If functions that add shows any answers-->

                <ul>
                    @foreach ($answers as $answer) <!--For each loop that shows each of the answers-->
                        <li><a href="/admin/answers/{{ $answer->id }}" name="{{ $answer->id }}">{{ $answer->option }}</a>{!! Session::has('msg') ? Session::get("msg") : '' !!}</li><!--Turns answers into links-->
                        <li><a href="/admin/answers/{{ $answer->id }}/edit" name="{{ $answer->id }}">Edit</a></li><!--Links that allows you to edit-->
                        <li>
                            {!! Form::open(['method' => 'DELETE', 'route' => ['answers.destroy', $answer->id]]) !!}<!--Button that deletes the answer-->
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                            {!! Form::close() !!}
                        </li>
                    @endforeach
                </ul>
             @else<!--Else no answers added-->
           <p> no answers added yet! </p><!--Message that tells you theres no answer-->
        @endif


    {{ Form::open(array('action' => 'AnswerController@create', 'method' => 'get')) }}<!--Button that takes user to the create answer page-->
        <div class="row">
            {!! Form::submit('Add Answer', ['class' => 'button']) !!}
        </div>
    {{ Form::close() }}

@endsection