@extends('layouts.app') <!--This calls the layout app for all the pages-->

@section('title', 'Answers') <!--This adds the title for the page-->

@section('content')<!--Inserts the content-->
<section>
    <h1>Added Answers</h1> <!--Title-->

     
        @if (isset ($answers)) <!--If functions that add shows any answers-->
                <ul>
                    @foreach ($answers as $answer)
                     <li><a href="/admin/answers/{{ $answer->id }}" name="{{ $answer->id }}">{{ $answer->option }}</a>{!! Session::has('msg') ? Session::get("msg") : '' !!}</li><!--Turns answers into links-->
                     <li><a href="/admin/answers/{{ $answer->id }}" name="{{ $answer->id }}">{{ $answer->option }}</a>{!! Session::has('msg') ? Session::get("msg") : '' !!}</li><!--Turns answers into links-->
                     <li><a href="/admin/answers/{{ $answer->id }}" name="{{ $answer->id }}">{{ $answer->option }}</a>{!! Session::has('msg') ? Session::get("msg") : '' !!}</li><!--Turns answers into links-->
                    @endforeach
                </ul>
            @else<!--Else no answers added-->
                <p> no answers added yet! </p><!--Gives a message that shows no answers added-->
        @endif
    

    <h2>Add another answer</h2><!--A second title-->

    {{ Form::open(array('action' => 'AnswerController@create', 'method' => 'get')) }}<!--Adds a button that takes the user to the create answer page-->
        <div class="row">
           {!! Form::submit('Add Answer', ['class' => 'button']) !!}
        </div>
    {{ Form::close() }}

</section> 

@endsection