@extends('layouts.app') <!--This calls the layout app for all the pages-->

@section('title', 'Create New Answer') <!--This adds the title for the page-->

@section('content') <!--Inserts the content-->

    <h1>Add Answer</h1> <!--Title-->

            <!-- errors -->
          @if ($errors->any())
              <div>
                  <ul class="alert alert-danger">
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif

        <!--Form that add the answer to the database-->
    {!! Form::open(array('action' => 'AnswerController@store', 'id' => 'createanswer')) !!}<!--Adds multiple elemnts to the answers for the question-->
            {{ csrf_field() }} <!--Secures the form and stops page from expiring-->
        <div>
        {!! Form::label('option', 'Add choice 1:') !!}
        {!! Form::text('option', $ans, null, ['class' => 'large-8 columns', 'multiple']) !!}<!--Adds multiple elemnts to the answers for the question-->
        </div>

        <div>
        {!! Form::label('option', 'Add choice 2:') !!}
        {!! Form::text('option', $ans, null, ['class' => 'large-8 columns', 'multiple']) !!}<!--Adds multiple elemnts to the answers for the question-->
        </div>

        <div>
        {!! Form::label('option', 'Add choice 3:') !!}
        {!! Form::text('option', $ans, null, ['class' => 'large-8 columns', 'multiple']) !!}<!--Adds multiple elemnts to the answers for the question-->
        </div>

        <div class="row large-4 columns">
            {!! Form::submit('Add Answer', ['class' => 'button']) !!}<!--Submits the form-->
        </div>
    {!! Form::close() !!}

@endsection