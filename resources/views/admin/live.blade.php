@extends('layouts.app')<!--This calls the layout app for all the pages-->

@section('title', 'Responses')<!--This adds the title for the page-->

@section('content')<!--Inserts the content-->

    @foreach($questionnaires as $questionnaire)<!--This is a for each that retrieves all of the questionnaires-->
        <h1>{{  $questionnaire->title  }}</h1> <!--Retrieves the title-->

        {!! Form::open(array('action' => 'ResponseController@store', 'method' => 'post')) !!}<!--Response form-->
            {{ csrf_field() }}<!--Secures the form and stops page from expiring-->

            @foreach($question as $question)<!--This is a for each that retrieves all of the questions-->

                <h2>{{$question->question_title}}<h2>

                    <ul>
                        @foreach($answer->answers as $answer)<!--This is a for each that retrieves all of the answers-->
                        <li>
                         {!! Form::label('submit_response', '{{ $answer->option }}') !!}<!--Creating the response-->
                         {!! Form::checkbox('submit_response', null, ['class' => 'large-8 columns']) !!}
                        </li>
                        @endforeach
                    </ul>
                 @endforeach

            <div class="row large-4 columns">
                {!! Form::submit('Add Response', ['class' => 'button']) !!}<!--submitting the response-->
            </div>
            @endforeach
        {!! Form::close() !!}

@endsection