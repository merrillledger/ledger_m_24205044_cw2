@extends('layouts.app')<!--This calls the layout app for all the pages-->

@section('title', 'Questions')<!--This adds the title for the page-->

@section('content')<!--Inserts the content-->

    <h1>Added questions</h1> 

        @if (isset ($question))<!--If functions that add shows any questions-->

            <ul>
                @foreach ($question as $question)<!--For each loop that shows each of the questions-->
                    <li><a href="/admin/question/{{ $question->id }}" name="{{ $question->id }}">{{ $question->question_title }}</a></li><!--Turns questions into links-->
                    <li><a href="/admin/question/{{ $question->id }}/edit" name="{{ $question->id }}">Edit</a></li><!--Links that allows you to edit-->
                    <li>
                      {!! Form::open(['method' => 'DELETE', 'route' => ['question.destroy', $question->id]]) !!}<!--Button that deletes the question-->
                      {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                      {!! Form::close() !!}
                    </li>
                @endforeach
            </ul>
         @else<!--Else no questions added-->
            <p> no questions added yet! </p><!--Message that tells you theres no questions-->
        @endif


    {{ Form::open(array('action' => 'QuestionController@create', 'method' => 'get')) }}<!--Button that takes user to the create questions page-->
        <div class="row">
            {!! Form::submit('Add Question', ['class' => 'button']) !!}
        </div>
    {{ Form::close() }}

@endsection